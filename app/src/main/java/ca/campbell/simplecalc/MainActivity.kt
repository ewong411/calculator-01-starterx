package ca.campbell.simplecalc

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

class MainActivity : Activity() {
    // Get a handle on the views in the UI
    // lateinit allows us to avoid using null initialization & !! & ?
    // but we must be sure to init before use otherwise
    // it acts as if !! was used & crashes on null value
    private lateinit var etNumber1: EditText
    private lateinit var etNumber2: EditText
    private lateinit var result1: TextView
    private final var TAG: String  = "CALC"
    internal var num1: Double = 0.toDouble()
    internal var num2: Double = 0.toDouble()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        etNumber1 = findViewById(R.id.num1) as EditText
        etNumber2 = findViewById(R.id.num2) as EditText
        result1 = findViewById(R.id.result1) as TextView
    } //onCreate()

    // TODO: replace with code that adds the two input numbers
    fun addNums(v: View) {
        if (etNumber1.text.toString() == "" && etNumber2.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = 0.toDouble()
            result1.setText(getString(R.string.firstmissing))
            Log.v(TAG, "User has failed to add the value of num1 and the value of num2" )
        }else if(etNumber1.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText(getString(R.string.secondmissing))
            Log.v(TAG, "User has failed to add the value of num1 and the value of num2" )

        }else if(etNumber2.text.toString() == ""){
            num2 = 0.toDouble()
            num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
            result1.setText(getString(R.string.thirdmissing))
            Log.v(TAG, "User has failed to add the value of num1 and the value of num2" )
        }else{
            num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText((num2 + num1).toString())
            Log.v(TAG, "User has added the value of num1: " + num1 + "and the value of num2: " + num2)

        }
        //  result.text = java.lang.Double.toString(2 * num1)    OK but not kotlin
    }  //addNums()

    fun minNums(y: View){
        if (etNumber1.text.toString() == "" && etNumber2.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = 0.toDouble()
            result1.setText(getString(R.string.firstmissing))
            Log.v(TAG, "User has failed to subtract value of num1 and the value of num2" )
        }else if(etNumber1.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText(getString(R.string.secondmissing))
            Log.v(TAG, "User has failed to subtract value of num1 and the value of num2" )
        }else if(etNumber2.text.toString() == ""){
            num2 = 0.toDouble()
            num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
            result1.setText(getString(R.string.thirdmissing))
            Log.v(TAG, "User has failed to subtract value of num1 and the value of num2" )
        }else {
            num1 = java.lang.Double.parseDouble((etNumber1.text.toString()))
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText((num1-num2).toString())
            Log.v(TAG, "User has subtracted the value of num1: " + num1 + "and the value of num2: " + num2)

        }
    }
    fun multiNum(y:View){
        if (etNumber1.text.toString() == "" && etNumber2.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = 0.toDouble()
            result1.setText(getString(R.string.firstmissing))
            Log.v(TAG, "User has failed to multiply value of num1 and the value of num2" )
        }else if(etNumber1.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText(getString(R.string.secondmissing))
            Log.v(TAG, "User has failed to multiply value of num1 and the value of num2" )
        }else if(etNumber2.text.toString() == ""){
            num2 = 0.toDouble()
            num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
            result1.setText(getString(R.string.thirdmissing))
            Log.v(TAG, "User has failed to multiply value of num1 and the value of num2" )
        }else {
            num1 = java.lang.Double.parseDouble((etNumber1.text.toString()))
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText((num1 * num2).toString())
            Log.v(TAG, "User has multiplied the value of num1: " + num1 + "and the value of num2: " + num2)
        }
    }
    fun divNum(y:View){
        if (etNumber1.text.toString() == "" && etNumber2.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = 0.toDouble()
            result1.setText(getString(R.string.firstmissing))
            Log.v(TAG, "User has failed to divide value of num1 and the value of num2" )
        }else if(etNumber1.text.toString() == ""){
            num1 = 0.toDouble()
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText(getString(R.string.secondmissing))
            Log.v(TAG, "User has failed to divide value of num1 and the value of num2" )
        }else if(etNumber2.text.toString() == ""){
            num2 = 0.toDouble()
            num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
            result1.setText(getString(R.string.thirdmissing))
            Log.v(TAG, "User has failed to divide value of num1 and the value of num2" )
        }else if(etNumber2.text.toString() == "0"){
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext,getString(R.string.divideby0),duration)
            toast.setGravity(Gravity.TOP or Gravity.LEFT,220,50)
            num2 = 0.toDouble()
            num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
            toast.show()
            result1.setText(getString(R.string.divideby0))
            Log.v(TAG, "User has failed to divide value of num1 and the value of num2" )
        }
        else {
            num1 = java.lang.Double.parseDouble((etNumber1.text.toString()))
            num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
            result1.setText((num1 / num2).toString())
            Log.v(TAG, "User has divided the value of num1: " + num1 + "and the value of num2: " + num2)
        }
    }
    fun clear(y:View){
        if(etNumber1.text.toString() != ""){
            etNumber1.setText(null)
        }
        if(etNumber2.text.toString() != ""){
            etNumber2.setText(null)
        }
        result1.setText(getString(R.string.result))
        Log.v(TAG, "User has cleared the input")
    }

}